const mongoose = require('mongoose')
const Schema = mongoose.Schema
const hourSchema = new Schema({
  type: String,
  mustCollected: Number,
  hoursCollected: Number
})
module.exports = mongoose.model('Hour', hourSchema)