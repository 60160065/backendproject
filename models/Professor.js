const mongoose = require('mongoose')
const Schema = mongoose.Schema
const professorSchema = new Schema({
  id: String,
  studentId: String,
  name: String,
  professor: String
})

module.exports = mongoose.model('Professor', professorSchema)