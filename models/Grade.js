const mongoose = require('mongoose')
const Schema = mongoose.Schema
const gradeSchema = new Schema({
  courseCode: String,
  courseName: String,
  grade: String
})

module.exports = mongoose.model('Grade', gradeSchema)