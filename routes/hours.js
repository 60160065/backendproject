const express = require('express');
const router = express.Router();
const hourController = require('../controller/HourController')

/* GET users listing. */
router.get('/', hourController.getHours)

module.exports = router;
