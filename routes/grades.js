const express = require('express');
const router = express.Router();
const gradeController = require('../controller/GradeController')

/* GET users listing. */
router.get('/', gradeController.getGrades)

module.exports = router;
