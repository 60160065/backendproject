const express = require('express');
const router = express.Router();
const professorController = require('../controller/ProfessorController')
const Professor = require('../models/Professor')

/* GET professors listing. */
router.get('/', professorController.getProfessors)

router.get('/:id', professorController.getProfessor)

router.put('/', professorController.updateProfessor)


module.exports = router;
