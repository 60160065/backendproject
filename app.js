var express = require('express')
var cors = require('cors')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var statusRouter = require('./routes/status')
var professorRouter = require('./routes/professor')
var hourRouter = require('./routes/hours')
var gradeRouter = require('./routes/grades')

var app = express()

var mongoose = require('mongoose')
mongoose.connect('mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/status', statusRouter)
app.use('/professors', professorRouter)
app.use('/hours', hourRouter)
app.use('/grades', gradeRouter)

module.exports = app
