const Hour = require('../models/Hour')
const hourController = {
  hourLists: [],
  async getHours(req, res, next) {
    try {
      const hours = await Hour.find({})
      res.json(hours)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = hourController