const Grade = require('../models/Grade')
const gradeController = {
  gradeLists: [],
  async getGrades(req, res, next) {
    try {
      const grades = await Grade.find({})
      res.json(grades)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = gradeController